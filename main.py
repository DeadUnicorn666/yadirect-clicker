from PyQt5 import QtWidgets, QtCore
import design
import sys
import logging
from Clicker import Clicker
from random import randint
import datetime
import time


class WorkerSignals(QtCore.QObject):
    result_signal = QtCore.pyqtSignal(str)
    message_signal = QtCore.pyqtSignal(str)
    proxies_signal = QtCore.pyqtSignal(str)


class Worker(QtCore.QRunnable):
    def __init__(self, startTime, endTime, clickSpan, websites, requests, captchaKey, proxyInFile, proxyOutFile):
        super(Worker, self).__init__()
        self.signals = WorkerSignals()

        now = datetime.datetime.now()
        self.start_time = datetime.datetime(year=now.year, month=now.month, day=now.day, hour=startTime[0],
                                            minute=startTime[1])
        self.end_time = datetime.datetime(year=now.year, month=now.month, day=now.day, hour=endTime[0],
                                          minute=endTime[1])
        self.click_span = clickSpan
        self.proxy_in_file = proxyInFile
        self.proxy_out_file = proxyOutFile
        self.proxies = []
        self.current_proxy_index = 0
        self.today_clicks = None
        self.clicker = Clicker(websites, requests, captchaKey)

    def calculate_today_clicks(self, timestamp_span):
        timestamp_span = (int(timestamp_span[0]), int(timestamp_span[1]))
        amount = randint(self.click_span[0], self.click_span[1])
        arr = [randint(timestamp_span[0] + 5, timestamp_span[1] - 5) for _ in range(amount)]
        arr[0] = timestamp_span[0] + 1
        self.today_clicks = sorted(arr)
        self.signals.message_signal.emit('Here\'s my clicking plan')
        for i in sorted(arr):
            self.signals.message_signal.emit(str(datetime.datetime.fromtimestamp(i)))

    def set_proxy(self, change=True):
        if self.current_proxy_index >= len(self.proxies):
            self.get_new_proxies()
            self.signals.message_signal.emit('Proxies has ended!!!')
            self.current_proxy_index = -1
        if change:
            if self.current_proxy_index != -1:
                with open(self.proxy_infile, 'w') as file:
                    file.write('\n'.join(self.proxies[self.current_proxy_index + 1:]))
                with open(self.proxy_out_file, 'a') as file:
                    file.write(self.proxies[self.current_proxy_index])
            self.current_proxy_index += 1
        self.clicker.set_proxy(self.proxies[self.current_proxy_index])
        self.signals.message_signal.emit('Current proxy is: ' + self.proxies[self.current_proxy_index])

    def get_new_proxies(self):
        with open(self.proxy_in_file) as file:
            self.proxies = file.read().split('\n')
        for proxy in self.proxies:
            self.signals.proxies_signal.emit(proxy)

    def run(self):
        now = datetime.datetime.now()
        delta = self.start_time - now
        delta = delta.total_seconds()
        if delta > 0:
            self.calculate_today_clicks((self.start_time.timestamp(), self.end_time.timestamp()))
            self.signals.message_signal.emit('I\'m gonna sleep for a while')
            time.sleep(delta)
        else:
            time_lost = (now - self.end_time).total_seconds() > 0
            if not time_lost:
                self.calculate_today_clicks((datetime.datetime.today().timestamp(), self.end_time.timestamp()))
            else:
                self.signals.message_signal.emit('today\'s click time is up')
                sys.exit(0)

        for click in self.today_clicks:
            delta = click - datetime.datetime.today().timestamp()
            if delta < 0:
                continue
            time.sleep(delta)
            is_successful = False
            error_counter = 0
            while not is_successful:
                if not error_counter % 3:
                    self.set_proxy()
                else:
                    self.set_proxy(False)
                result = self.clicker.click()
                is_successful = result['status']
                self.clicker.close_browser()
            if result['status']:
                result_string = 'OK ' + result['url']
            else:
                result_string = 'error'
            self.signals.result_signal.emit(result_string)


class ClickerApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        logging.basicConfig(filename='direct_clicker.log', level=logging.INFO)
        self.setupUi(self)

        self.requestsList.setPlainText('купить реквизит для фокусов')
        self.websitesList.setPlainText('magiclesson-shop.ru')
        self.minClicks.setValue(5)
        self.maxClicks.setValue(10)
        self.endTime.setTime(QtCore.QTime(15, 0))

        self.last_proxy_used = 0
        self.startBtn.clicked.connect(self.start_clicking)
        self.threadpool = QtCore.QThreadPool()

    def write_result(self, result):
        self.proxyTable.setItem(self.last_proxy_used, 1, QtWidgets.QTableWidgetItem(result))

    def add_proxy(self, proxy):
        if ':' in proxy:
            proxy = proxy[:proxy.index(':')]
        last_line = self.proxyTable.rowCount()
        self.proxyTable.insertRow(last_line)
        self.proxyTable.setItem(last_line, 0, QtWidgets.QTableWidgetItem(proxy))

    def log(self, message):
        logging.info(message)
        self.listWidget.addItem(message)

    def start_clicking(self):
        requests = self.requestsList.toPlainText().split('\n')
        websites = self.websitesList.toPlainText().split('\n')

        startTime = self.startTime.time()
        startTime = startTime.hour(), startTime.minute()
        endTime = self.endTime.time()
        endTime = endTime.hour(), endTime.minute()

        minClicks = self.minClicks.value()
        maxClicks = self.maxClicks.value()

        captchaKey = self.captchaKey.text()

        proxyInFile = 'proxies.txt'
        proxyOutFile = 'used_proxies.txt'

        worker = Worker(startTime, endTime, (minClicks, maxClicks), websites, requests, captchaKey, proxyInFile,
                        proxyOutFile)
        worker.signals.result_signal.connect(self.write_result)
        worker.signals.message_signal.connect(self.log)
        worker.signals.proxies_signal.connect(self.add_proxy)
        self.threadpool.start(worker)


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = ClickerApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
