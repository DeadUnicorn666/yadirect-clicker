from Clicker import Clicker
import datetime
import time
import sys
from random import randint


class Supervisor:
    def __init__(self, start_time,  end_time, click_span, wanted_websites, wanted_requests, captcha_api_key,
                 proxy_in_file, proxy_out_file):
        now = datetime.datetime.now()
        self.start_time = datetime.datetime(year=now.year, month=now.month, day=now.day, hour=start_time[0],
                                            minute=start_time[1])
        self.end_time = datetime.datetime(year=now.year, month=now.month, day=now.day, hour=end_time[0],
                                          minute=end_time[1])
        self.click_span = click_span
        self.proxy_in_file = proxy_in_file
        self.proxy_out_file = proxy_out_file
        with open(proxy_in_file) as file:
            self.proxies = file.read().split('\n')
        self.current_proxy_index = -1
        self.today_clicks = None
        self.clicker = Clicker(wanted_websites, wanted_requests, captcha_api_key)

    def calculate_today_clicks(self, timestamp_span):
        timestamp_span = (int(timestamp_span[0]), int(timestamp_span[1]))
        print(timestamp_span)
        amount = randint(self.click_span[0], self.click_span[1])
        arr = [randint(timestamp_span[0] + 5, timestamp_span[1] - 5) for _ in range(amount)]
        arr[0] = timestamp_span[0] + 1
        self.today_clicks = sorted(arr)
        for i in sorted(arr):
            print(datetime.datetime.fromtimestamp(i))

    def set_proxy(self, change=True):
        if self.current_proxy_index != -1:
            with open(self.proxy_out_file, 'a') as file:
                file.write(self.proxies[self.current_proxy_index])
        if change:
            self.current_proxy_index += 1
        self.clicker.set_proxy(self.proxies[self.current_proxy_index])
        print('Current proxy is:', self.proxies[self.current_proxy_index])
        if self.current_proxy_index >= len(self.proxies):
            print('Proxies has ended!!!')
            self.current_proxy_index = 0

    def start(self):
        now = datetime.datetime.now()
        delta = self.start_time - now
        delta = delta.total_seconds()
        if delta > 0:
            self.calculate_today_clicks((self.start_time.timestamp(), self.end_time.timestamp()))
            print('I\'m gonna sleep for a while')
            time.sleep(delta)
        else:
            time_lost = (now - self.end_time).total_seconds() > 0
            if not time_lost:
                self.calculate_today_clicks((datetime.datetime.today().timestamp(), self.end_time.timestamp()))
            else:
                print('today\'s click time is up')
                sys.exit(0)

        for click in self.today_clicks:
            delta = click - datetime.datetime.today().timestamp()
            if delta < 0:
                continue
            time.sleep(delta)
            is_successful = False
            error_counter = 0
            while not is_successful:
                if not error_counter % 3:
                    self.set_proxy()
                else:
                    self.set_proxy(False)
                try:
                    result = self.clicker.click()
                    is_successful = result['status']
                    self.clicker.close_browser()
                except:
                    error_counter += 1
