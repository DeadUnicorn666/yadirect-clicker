import requests
import shutil
import base64
from selenium.webdriver import Firefox, FirefoxProfile
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
import time
import os
import json
from random import choice


class Clicker:
    def __init__(self, websites, wanted_requests, captcha_api_key):
        self.browser = None
        self.wanted_websites = websites
        self.search_requests = wanted_requests
        self.captcha_api_key = captcha_api_key

    def set_proxy(self, proxy):
        ip, port, protocol = proxy.split(':')
        profile = FirefoxProfile()
        if 'SOCKS' in protocol:
            profile.set_preference("network.proxy.type", 1)
            profile.set_preference("network.proxy.socks", ip)
            profile.set_preference("network.proxy.socks_port", int(port))
            profile.set_preference("network.proxy.socks_version", int(protocol[-1]))
        elif 'HTTP' in protocol:
            profile.set_preference("network.proxy.type", 1)
            profile.set_preference("network.proxy.http", ip)
            profile.set_preference("network.proxy.http_port", int(port))
        profile.update_preferences()

        self.browser = Firefox(firefox_profile=profile)

    def make_request(self):
        self.browser.get('https://yandex.ru')
        search_input = WebDriverWait(self.browser, 10).until(expected_conditions.presence_of_element_located(
            (By.ID, 'text')))
        search_input.send_keys(choice(self.search_requests))
        search_input.submit()
        start = time.time()
        while True:
            if 'yandex.ru/showcaptcha' in self.browser.current_url:
                self.solve_captcha()
                break
            elif 'yandex.ru/search' in self.browser.current_url:
                break
            else:
                if time.time() - start >= 10:
                    raise Exception
                time.sleep(0.3)

    def solve_captcha(self):
        captcha = self.browser.find_element_by_class_name('form__captcha')
        image_url = captcha.get_attribute('src')
        r = requests.get(image_url, stream=True)
        if r.status_code == 200:
            with open('captcha.gif', 'wb') as f:
                r.raw.decode_content = True
                shutil.copyfileobj(r.raw, f)

        with open('captcha.gif', 'rb') as file:
            encoded = base64.b64encode(file.read())
        r = requests.post('http://2captcha.com/in.php', data=json.dumps({'key': self.captcha_api_key, 'method': 'base64',
                                                                         'body': encoded.decode('utf-8'), 'json': 1}))
        captcha_id = r.json()['request']
        result = 'CAPCHA_NOT_READY'
        while result == 'CAPCHA_NOT_READY':
            time.sleep(3)
            r = requests.get('http://2captcha.com/res.php', params={'key': self.captcha_api_key, 'action': 'get',
                                                                    'id': captcha_id, 'json': 1})
            result = r.json()['request']
        res_input = self.browser.find_element_by_id('rep')
        res_input.send_keys(result)
        res_input.submit()
        os.remove('captcha.gif')

    def click_wanted(self):
        WebDriverWait(self.browser, 10).until(expected_conditions.presence_of_all_elements_located(
            (By.CLASS_NAME, 'serp-item')))
        arr = self.browser.find_elements_by_class_name('serp-item')
        is_clicked = False
        clicked_website = None
        for item in arr:
            if item.find_elements_by_class_name('organic__label_align_right'):
                el = item.find_element_by_class_name('organic__path').find_element_by_tag_name('b')
                clicked_website = el.text
                if el.text in self.wanted_websites:
                    item.find_element_by_tag_name('a').click()
                    is_clicked = True
                    break
        if is_clicked:
            start = time.time()
            while True:
                if len(self.browser.window_handles) == 2:
                    break
                else:
                    if time.time() - start >= 10:
                        raise Exception
                    time.sleep(0.3)
        return dict(url=clicked_website, status=is_clicked)

    def click(self):
        self.make_request()
        res = self.click_wanted()
        return res

    def close_browser(self):
        self.browser.close()
        for window in self.browser.window_handles:
            self.browser.switch_to.window(window)
            self.browser.close()


class UnsolvableCaptchaError(Exception):
    def __init__(self, message):
        super().__init__(message)
